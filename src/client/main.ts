import { drawField, drawSnakes } from './gameField';
import { ISnakeDirection } from '../types';

// eslint-disable-next-line prefer-destructuring
const NODE_ENV = process.env.NODE_ENV;
let wsUrl = `${document.location.protocol.replace('http', 'ws')}//${document.location.host}/ws`;
if (NODE_ENV === 'development') {
  wsUrl = 'ws://127.0.0.1:8999/ws';
}

const connection = new WebSocket(wsUrl);

connection.addEventListener('open', (event: Event) => {
  console.log('WebSocket-open', event);
});

connection.addEventListener('close', (event: CloseEvent) => {
  console.log('WebSocket-close', event);
});

connection.addEventListener('error', (error: Event) => {
  console.error('WebSocket-error', error);
  setTimeout(() => {
    document.location.reload();
  }, 1000);
});

connection.addEventListener('message', (message: MessageEvent) => {
  console.log('WebSocket-message', message);

  try {
    const json = JSON.parse(message.data);
    console.log('json', json);
    if (!json.id) {
      return;
    }

    switch (json.action) {
      case 'start':
        const $id = document.getElementById('id');
        if ($id) {
          $id.innerHTML = json.id;
        }
        drawField(json.sizeX, json.sizeY);
        break;
      case 'update':
        const $direction = document.getElementById('direction');
        if ($direction) {
          $direction.innerHTML = json.direction || 'xxx';
        }
        const $color = document.getElementById('color');
        if ($color) {
          $color.style.backgroundColor = json.color;
        }
        console.log('json', json);
        drawSnakes({ snakes: json.snakes });
        break;
      default:
        console.log('ERROR-unknown-action', json.action, json);
    }
  } catch (e) {
    console.error(e, message.data);
  }
});

window.addEventListener('keydown', (event) => {
  const { key } = event;
  let direction: ISnakeDirection | undefined;
  if (key.toLocaleLowerCase() === 'a' || key === 'ArrowLeft') {
    direction = 'left';
  } else if (key.toLocaleLowerCase() === 'd' || key === 'ArrowRight') {
    direction = 'right';
  } else if (key.toLocaleLowerCase() === 'w' || key === 'ArrowUp') {
    direction = 'up';
  } else if (key.toLocaleLowerCase() === 's' || key === 'ArrowDown') {
    direction = 'down';
  }

  if (direction) {
    connection.send(
      JSON.stringify({
        action: 'change_direction',
        direction,
      }),
    );
  }
});
